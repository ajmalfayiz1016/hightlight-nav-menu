const sections = document.querySelectorAll('section');
const navLi = document.querySelectorAll('header nav ul li')

window.addEventListener('scroll',function(){
    let current = '';
    sections.forEach(section=> {
        const sectionTop = section.offsetTop;
        // console.log(sectionTop);
        const sectionHeight = section.clientHeight;
        // console.log(sectionHeight);
        if(scrollY >= sectionTop-sectionHeight / 3){
            current = section.getAttribute('id');
        }
        console.log(current);
        navLi.forEach(li=>{
            li.classList.remove('active');
            if(li.classList.contains(current)){
                li.classList.add('active');
            }
        })
    })
})